(ns #^{:author "Eric Schulte",
       :license "GPLV3",
       :doc "Simple concurrent propagator system."}
  propagator
  (:use clojure.contrib.repl-utils clojure.contrib.math))

(defn set-cell "Set the value of a cell" [cell value]
  (send cell (constantly value)))

(defmacro run-propagator
  "Run a propagator, first collect the most recent values from all
cells associated with the propagator, then evaluate."
  [propagator]
  `(let [results# (apply ~propagator (map deref (:in-cells (meta ~propagator))))]
     (doseq [cell# (:out-cells (meta ~propagator))]
       (when (not (= @cell# results#))
         (set-cell cell# results#)))
     results#))

(defn add-neighbor "Add a neighbor to the given cell." [cell neighbor]
  (add-watch cell nil (fn [_ _ _ _] (future (run-propagator neighbor)))))

(defmacro defcell "Define a new cell." [name state]
  `(def ~name (agent ~state)))

(defmacro defpropagator "Define a new propagator."
  [name in-cells out-cells & body]
  `(let [v# (defn ~(vary-meta name assoc :in-cells in-cells :out-cells out-cells)
              ~in-cells ~@body)]
     (doseq [cell# ~in-cells] (add-neighbor cell# ~name))
     v#))
