
(in-ns 'propagator)
(cell guess 1)
(cell x 9)
(cell done false)
(cell margin 0.1)

(propagator enough [x guess] [done]
  (Thread/sleep 1000)
  (if (< (abs (- (* guess guess) x)) @margin) true false))

(propagator heron [x done guess] [guess]
  (Thread/sleep 1000)
  (if done
    guess
    (/ (+ guess (/ x guess)) 2.0)))
