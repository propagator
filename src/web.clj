
(load-file "/home/eschulte/src/propagator/src/propagator.clj")
(in-ns 'propagator)
(use 'ring.adapter.jetty)
(use 'clojure.contrib.seq-utils)
(import 'java.util.Date 'java.text.SimpleDateFormat)

;; cells
(cell names '())
(cell input "")

(propagator adder [input names] [names]
            (when (> (count (seq input)) 0)
              (set (cons (str input " :1") names))))

(defn app [req]
  (or
   ;; page to accept input
   (when (= "/" (:uri req))
     {:status 200
      :headers {"Content-Type" "text/html"
                "Title" "3000"}
      :body (apply str
                   "<form name=\"\" action=\"add\" method=\"get\">"
                   "Word: <input type=\"type\" name=\"word\" />"
                   "<input type=\"submit\" value=\"Submit\" />"
                   "</form>")})
   ;; dump value into "input" cell
   (when (re-matches #"/add" (:uri req))
     (set-cell input (second (re-matches #".+=(.+)" (:query-string req))))
     {:status 303 :headers {"Location" "../list"}})
   ;; render the value of the "list" cell
   (when-let [matched (re-matches #"/list" (:uri req))]
     {:status  200
      :headers {"Content-Type" "text/html"}
      :body    (apply str (flatten (list "<ul>"
                                         (map #(str "<li>"%"</li>") @names)
                                         "</ul>"
                                         "<p><a href=\"/\">another word</a></p>")))})))

(run-jetty #'app {:port 3000})
