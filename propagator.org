#+TITLE: Concurrent Propagator in Clojure
#+OPTIONS: toc:nil num:nil
#+LaTeX_CLASS: normal
#+begin_html
  <style type="text/css">
    <!--/*--><![CDATA[/*><!--*/
    pre { background-color: #232323; color: #E6E1DC;}
    /*]]>*/-->
  </style>
#+end_html

A concurrent propagator system implemented in [[http://clojure.org][Clojure]].  This simple
project builds on the scheme propagator system from Gerald Sussman's
[[http://dspace.mit.edu/handle/1721.1/44215][The art of the Propagator]].

#+begin_quote
  We develop a programming model built on the idea that the basic
  computational elements are autonomous machines interconnected by
  shared cells through which they communicate. Each machine
  continuously examines the cells it is interested in, and adds
  information to some based on deductions it can make from information
  from the others. This model makes it easy to smoothly combine
  expression-oriented and constraint-based programming; it also easily
  accommodates implicit incremental distributed search in ordinary
  programs. This work builds on the original research of Guy Lewis
  Steele Jr. and was developed more recently with the help of Chris
  Hanson.
#+end_quote

Major differences between this system and the one implemented in /The
art of the Propagator/ are that,
1) this implementation admits parallel execution through the use of
   Clojure's [[http://clojure.org/agents][agents]].
2) this version does not implement the backtracking system which
   breaks the locality of the propagator system

(note: this is an exploratory, educational implementation and isn't
suitable for any sort of /production/ application.)

#+source: code-counter
#+begin_src sh :var file="src/propagator.clj" :exports none
  cat $file|sed '/^[ \t]*$/d'|grep -v "^[ \t];"|wc -l
#+end_src

This system is implemented in src_emacs-lisp[:var d=code-counter]{d}
lines of Clojure code in [[http://repo.or.cz/w/propagator.git/blob_plain/HEAD:/src/propagator.clj][src/propagator.clj]].

** Usage Examples
- square roots calculation using Heron's method
  #+begin_src clojure :tangle src/heron.clj
    (in-ns 'propagator)
    (defcell guess 1)
    (defcell x 9)
    (defcell done false)
    (defcell margin 0.1)
    
    ;; check if we're close enough to a solution to cease improving
    (defpropagator enough [x guess] [done]
      (Thread/sleep 1000) ; sleep to allow observation of incremental calculation
      (if (< (abs (- (* guess guess) x)) @margin) true false))
    
    ;; incrementally improve our guess
    (defpropagator heron [x done guess] [guess]
      (Thread/sleep 1000)
      (if done
        guess
        (/ (+ guess (/ x guess)) 2.0)))
    
    (comment           ; after building the system
      (set-cell x 89)  ; update the value of x
      (deref guess)    ; immediately begin observing improvements in guess
      (deref guess))
  #+end_src

- parallel spread sheet using Clojure formulas is implemented in
  src_emacs-lisp[:var d=code-counter(file="src/spreadsheet.clj")]{(- d 5)}
  in [[http://repo.or.cz/w/propagator.git/blob_plain/HEAD:/src/spreadsheet.clj][src/spreadsheet.clj]].

** License
Copyright (C) 2010 Eric Schulte

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
