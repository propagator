(defproject propagator "1.0.0-SNAPSHOT"
  :description "Concurrent propagator system"
  :dependencies [[org.clojure/clojure "1.2.0"]
                 [org.clojure/clojure-contrib "1.2.0"]
                 [vijual "0.1.0-SNAPSHOT"]]
  :dev-dependencies [[swank-clojure "1.2.1"]])